package com.sda.jsp.model;

import javax.persistence.*;
import java.time.LocalDate;

@Table
@Entity(name = "movie")
public class Movie {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;


    @OneToOne(fetch = FetchType.EAGER)
    private Person director;

    @Column
    private LocalDate releaseDate;

    @Column
    private double rate;

    public Movie() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Person getDirector() {
        return director;
    }

    public void setDirector(Person director) {
        this.director = director;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        return "Title: " + name + '\'' +
                ", director: " + director +
                ", releaseDate: " + releaseDate +
                ", rate: " + rate ;
    }
}

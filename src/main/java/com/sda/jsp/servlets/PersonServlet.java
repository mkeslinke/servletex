package com.sda.jsp.servlets;

import com.sda.jsp.dao.MoviesDao;
import com.sda.jsp.dao.PersonDao;
import com.sda.jsp.model.Movie;
import com.sda.jsp.model.Person;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class PersonServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        PersonDao dao = new PersonDao();
        try {
            List<Person> persons = dao.getAllPersons();
            request.setAttribute("persons", persons);
            request.getRequestDispatcher("/persons.jsp").forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
//
//    @Override
//    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//
//
//    }
}

package com.sda.jsp.servlets;

import com.sda.jsp.dao.MoviesDao;
import com.sda.jsp.model.Movie;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;


@WebServlet(urlPatterns = "/movies")
public class MovieServlet extends HttpServlet{

    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        MoviesDao dao = new MoviesDao();

        try {

            List<Movie> movies = dao.getAllMovies();
//            PrintWriter writer = response.getWriter();
//            writer.println("<h1> Hello World!</h1>");
//
//            writer.flush();

            request.setAttribute("movies",movies);
            request.getRequestDispatcher("/movies.jsp").forward(request,response);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ServletException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
        String title = request.getParameter("title");
        String dir = request.getParameter("director");
        System.out.println(request.getParameter("rate"));
        Double rate = Double.parseDouble(request.getParameter("rate"));

        String releaseDate = request.getParameter("releaseDate");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        Movie movie = new Movie();
        movie.setName(title);
        movie.setRate(rate);
        movie.setReleaseDate(LocalDate.parse(releaseDate, formatter));

        MoviesDao moviesDao = new MoviesDao();
        moviesDao.saveMovie(movie);
        resp.sendRedirect("/movies");
    }
}

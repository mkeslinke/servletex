package com.sda.jsp.dao;

import com.sda.jsp.model.Movie;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class MoviesDao {
    public List<Movie> getAllMovies(){
        Session session = DatabaseSession.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Criteria criteria = session.createCriteria(Movie.class);
        List<Movie> moviesList = criteria.list();

        return moviesList;

    }


    public void putMockObject() {
        Session session = DatabaseSession.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(new Movie());

        transaction.commit();
        session.close();
    }

    public void saveMovie(Movie movie){
        Session session = DatabaseSession.getSessionFactory().openSession();
        session.beginTransaction();
        session.saveOrUpdate(movie);
        session.flush();
        session.getTransaction().commit();
        session.close();

    }
}

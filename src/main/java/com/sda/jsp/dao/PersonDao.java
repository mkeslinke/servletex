package com.sda.jsp.dao;

import com.sda.jsp.model.Person;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class PersonDao {
    public List<Person> getAllPersons(){
        Session session = DatabaseSession.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Criteria criteria = session.createCriteria(Person.class);
        List<Person> personList = criteria.list();

        return personList;
    }



    public void safePerson(Person person){
    Session session = DatabaseSession.getSessionFactory().openSession();
        session.beginTransaction();
        session.saveOrUpdate(person);
        session.flush();
        session.getTransaction().commit();
        session.close();
    }
}

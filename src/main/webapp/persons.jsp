<%@ page import="com.sda.jsp.model.Person" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Marcin
  Date: 11.03.2018
  Time: 14:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Persons</title>

    <jsp:include page="views/includes.jsp"></jsp:include>
</head>
<body>
<jsp:include page="views/header.jsp"></jsp:include>


<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 bg-light">
    <div class="col-md-10 p-lg-10 mx-auto my-10">
        <h1 class="font-weight-normal">Persons</h1>

        <div class="row col-lg-12">
            <div class="col-md-1"><h4>Id</h4></div>
            <div class="col-md-3"><h4>First name</h4></div>
            <div class="col-md-1"><h4>Surname</h4></div>
            <div class="col-md-2"><h4>Date of Birth</h4></div>
            <div class="col-md-1"><h4>Rate</h4></div>
            <div class="col-md-2"><h4>Proffesion</h4></div>

        </div>
        <select>
        <%
            List<Person> personList = (List<Person>) request.getAttribute("persons");


            for (Person person: personList){
                out.println("<option value=\""+person.getId()+"\" name=\""+person.getFirstName()+" " + person.getSurname()+

            }
          </select>




            StringBuilder builder = new StringBuilder();

            int i = 0;
            for (Person p : personList) {
                if (i % 2 == 0) {
                    builder.append("<div class=\"row white\">");
                } else {
                    builder.append("<div class=\"row black\">");
                }
                builder.append("<div class=\"col-md-1\">").append(p.getId()).append("</div>");
                builder.append("<div class=\"col-md-3\">").append(p.getFirstName()).append("</div>");
                builder.append("<div class=\"col-md-1\">").append(p.getSurname()).append("</div>");
                builder.append("<div class=\"col-md-2\">").append(p.getDateOfBirth()).append("</div>");
                builder.append("<div class=\"col-md-1\">").append(p.getRate()).append("</div>");
                builder.append("<div class=\"col-md-1\">").append(p.getProfession()).append("</div>");
                builder.append("</div>");
                i++;
            }
            out.print(builder.toString());
        %>
    </div>
</div>


<jsp:include page="views/footer.jsp"></jsp:include>
</body>
</html>
